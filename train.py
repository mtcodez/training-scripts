import json
import numpy as np
import matplotlib.pyplot as plt
import numpy as np
from sklearn import datasets, linear_model
from sklearn import neural_network
from sklearn.svm import SVR
import subprocess
import sys

training_metrics = ['cpu_system', 'cpu_user', ]

def download(host, databse, length, tag, output):
	filename = output + '/' + tag
	with open(filename, 'wb') as handle:
	    command = 'curl -s -k -G "http://' + host + '/query" --data-urlencode "db=' + databse + '" --data-urlencode "q=select value from /.*/ where time >= now() - ' + length + "m AND jvb = \'" + tag + "'\""
	    print command
	    process = subprocess.Popen(command, shell=True, stdout=handle)
	    process.wait()
	return filename

# parse json
def parseInfluxDbData(influxdbFile):
	# read to python objects
	with open(influxdbFile) as json_data:
	    d= json.load(json_data)

	series = d['results'][0]['series']

	metrics = []
	sizes = []

	# remove unused columns
	metrics.append('timestamp')
	for s in series:
		if s['name'] == 'conference_sizes':
			continue
		metrics.append(s['name'])
		sizes.append(len(s['values']))

	# allocate lists for later usage
	processed = [[] for x in  xrange(max(sizes))] # [[], [], [], ...]
	firstMaxItem = sizes.index(max(sizes))

	print metrics, sizes
	# append timestamp
	for f in xrange(0, max(sizes)):
		processed[f].append(series[firstMaxItem]['values'][f][0])

	max_size = max(sizes)
	for s in series:
		if s['name'] == 'conference_sizes':
			continue
		i = 0 # specific metric
		j = 0 # biggest metric
		# print s['name'], len(s['values'])
		while i < len(s['values']): # less than a specific metric's size
			# print i, j, s['values'][i][0][:s['values'][i][0].find('.')], processed[j][0][:processed[j][0].find('.')]
			# compare timestamp
			if s['values'][i][0][:s['values'][i][0].find('.')] == processed[j][0][:processed[j][0].find('.')]:
				if s['values'][i][1] == None:
					processed[j].append(0)
				else:
					processed[j].append(s['values'][i][1])
				j = j + 1
				i = i + 1
			else:
				processed[j].append(0)
				j = j + 1
	return processed, metrics



def averageProcessed(processed, metrics):
	output = []
	begin = 0	
	end = 0
	conference = int(processed[0][metrics.index('participants')])
	while(True):
		if end == len(processed)-1:
			break
		old = conference
		conference = int(processed[end][metrics.index('participants')]) # 0 is timestamp
		if old == conference:
			end = end + 1
		else:
			# average then
			block = np.array([processed[x][1:] for x in xrange(begin, end)])
			# print begin, end, conference, old
			output.append(np.mean(block, axis=0).tolist())
			begin = end
	return output

# skip []
def prepareData(avergaedData, metrics, predictor):
	leftSide = []
	rightSide = []
	participantsIndex = metrics.index('participants')-1
	print 'predictor:', predictor
	predictIndex = metrics.index(predictor)-1
	for i in xrange(2, len(avergaedData)):
		# another dataset
		if avergaedData[i] == []:
			i = i + 1
			continue

		diff = avergaedData[i][participantsIndex] - avergaedData[i-1][participantsIndex]
		# if avergaedData[i][participantsIndex] == 0:
		# 	continue
		# 
		leftSide.append(avergaedData[i][predictIndex]) #  after change
		before = list(avergaedData[i-1])
		# del before[metrics.index(predictor)-1]
		before.append(diff)
		# before.append(avergaedData[i][metrics.index('videostreams')-1] - avergaedData[i-1][metrics.index('videostreams')-1])
		rightSide.append(before) # everything before change except cpu
	# print leftSide[0], rightSide[0]
	return leftSide, rightSide

def getMetricsList(input_file):
	with open(input_file) as json_data:
	    d = json.load(json_data)

	series = d['results'][0]['series']

	metrics = []
	sizes = []

	# remove unused columns
	metrics.append('timestamp')
	for s in series:
		if s['name'] == 'conference_sizes':
			continue
		metrics.append(s['name'])
		sizes.append(len(s['values']))
	return metrics

def train(x, y):
	xTrain = x[:-20]
	xTest = x[-20:]
	yTrain = y[:-20]
	yTest = y[-20:]

	lm = linear_model.Lasso(alpha = 0.05, max_iter=50000000)
	# lm = linear_model.Ridge(alpha = 0.5)
	# lm = neural_network.MLPRegressor()
	# lm = SVR(kernel='rbf', C=1e3, gamma=0.1)
	lm.fit(xTrain, yTrain)
	# print("Coefficients: ", lm.coef_)
	print("Residual sum of squares: %.2f"
      % np.mean((lm.predict(xTest) - yTest) ** 2))
	# Explained variance score: 1 is perfect prediction
	print("R Squared: %.2f" % lm.score(xTest, yTest))
	# plt.scatter(np.array(x), np.array(y))

if __name__ == '__main__':
	training_files = ['micro/10-I.json', 'micro/11-I.json']

	averagedData = []
	metrics = getMetricsList(training_files[0])
	for f in training_files:		
		processed, metrics = parseInfluxDbData(f)
		output= averageProcessed(processed, metrics)
		averagedData.extend(output)
		averagedData.extend([]) # seperator between different datasets

	leftSide, rightSide = prepareData(averagedData, metrics, sys.argv[1])
	train(rightSide, leftSide)
	# download('54.187.56.227:8086', 'metrics', '20', 'jitsi-videobridge02.ip-172-31-42-182.us-west-2.compute.internal', ".")
