#!/bin/bash

function parse_options {
  function usage() {
    echo -e >&2 "Usage: $0 dump DATABASE [options...]
\t-u USERNAME\t(default: admin)
\t-p PASSWORD\t(default: admin)
\t-h HOST\t\t(default: localhost:8086)
\t-s\t\t(use HTTPS)
\t-a STARTTIME
\t-e ENDTIME
\t-j JVBNUMBER"
  }
  if [ "$#" -lt 2 ]; then
    usage; exit 1;
  fi

  username=admin
  password=admin
  host=localhost:8086
  https=0
  shift
  database=$1
  shift

  while getopts u:p:h:s:a:e:j: opts
  do case "${opts}" in
    u) username="${OPTARG}";;
    p) password="${OPTARG}";;
    h) host="${OPTARG}";;
    s) https=1;;
    a) start="${OPTARG}";;
    e) end="${OPTARG}";;
    j) jvb="${OPTARG}";;
    ?) usage; exit 1;;
    esac
  done
  if [ "${https}" -eq 1 ]; then
    scheme="https"
  else
    scheme="http"
  fi
}

function dump {
  parse_options $@
  jvb_str=\'${jvb}\'
  start_str=\'${start}\'
  end_str=\'${end}\'
  curl -s -k -G  "${scheme}://${host}/query" --data-urlencode "db=${database}" --data-urlencode "q=select value from /.*/ WHERE time >= ${start_str} AND time <= ${end_str} AND jvb = ${jvb_str}" \
    | jq . -c -M
  exit
}

function restore {
  parse_options $@

  while read -r line
  do
    echo >&2 "Writing..."
    curl -X POST -d "[${line}]" "${scheme}://${host}/db/${database}/series?u=${username}&p=${password}"
  done
  exit
}

case "$1" in
  dump)     dump $@;;
  restore)  restore $@;;
  *)      echo >&2 "Usage: $0 [dump|restore] ..."
    exit 1;;
esac
